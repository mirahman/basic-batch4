<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
echo "<pre>";

$name = "outside class";



Class Fruit {
    
    var $name = "fruit";
    
    
    function showName()
    {
        echo "I am ".$this->name;
    }
    
}

Class Flower {
    
    var $name = "flower";
    
    
    function showName()
    {
        return "I am ".$this->getName();
    }
    
    function getName()
    {
        return $this->name;
    }
    
}

$mango = new Fruit();
$rose  = new Flower();
$lily = new Flower();


echo $mango->name."<br />";
$mango->showName();
echo "<br >";

echo $rose->name."<br />";
echo $rose->showName()."<br />";

echo $lily->name."<br />";
echo $lily->showName()."<br />";

var_dump($mango);
var_dump($rose);
var_dump($lily);
        