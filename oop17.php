<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
abstract  class fruits {
    abstract public function taste();
}
class Mango extends fruits{
    
    public function taste() {
        echo "I am Mango";
    }
}


class Banana  extends fruits{
    public function taste() {
        echo "I am Banana";
    }
}

class Guava extends fruits{
    public function taste() {
        
    }
}

$lengra = new Mango;
$sobri   = new Banana;
$kazi    = new Guava;

$arr = [];
$arr[] = $lengra;
$arr[] = $sobri;
$arr[] = $kazi;

foreach($arr as $obj)
{
    $obj->taste();
}

