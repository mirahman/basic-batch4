<?php

// please override all methods in child class

trait Fruit{
        
    public $int;
    
    public function setName() {
        echo "I am setting name";
    }
    
    public function showName() {
        echo "i am from showing name";
        $this->setName();
    }
    
}

trait Mango {
    
    public function showTaste() {
        echo "Very tasty";
    }
    
    public function showName() {
        echo "i am from showing mango name";
        $this->setName();
    }
    
}


class Lengra {
    
    use Fruit, Mango {
      Fruit::showName insteadof Mango;
      Mango::showName as mangoShowName;
    }

    function showTaste() {
        echo "I am lengra the king of mango";
    }
}

$mango = new Lengra();
$mango->mangoShowName();