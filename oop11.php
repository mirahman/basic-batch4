<?php

class Fruit  {
    
    public function myName() {
        self::getName();
        //$this->getName();
    }
    
    
    public function getName() {
        echo " i am Fruit <br />";
    }
}

class Mango extends Fruit {
        
    public function getName() {
        echo " i am Mango <br />";
    }
}


$cric = new Mango();
$cric->myName();

$abc = new Fruit();
$abc->myName();
