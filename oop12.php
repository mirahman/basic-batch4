<?php

// please override all methods in child class

abstract class Fruit  {
    
    public function myName() {
        //self::getName();
        //$this->getName();
    }
    
    
    public function getName() {
        //echo " i am Fruit <br />";
    }
    
    abstract public function setName();
}

abstract class Mango extends Fruit {
        
 abstract public  function showName();
}

class Lengra extends Mango {
    
    public function setName() {
        
    }
    
    public function showName() {
        
    }
    
}

//$mango = new Mango();