<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// shallow copy
echo "<pre>";

Class Fruit {
    
    public $name = "";
    public $origin = "";
    protected $taste = 0;
    
    public $productionYear = "";
    
    
    function __construct($name = "1", $origin ="2", $taste ="3") {
        //echo "I am being called with $name, $origin, $taste <br />";
        $this->name = $name;
        $this->origin = $origin;
        $this->taste = $taste;
    }
    
    function getName() {
        $this->validate();
        
        return $this->name;
        
    }
    
    function setName($name) {
        $this->name = $name;
    }

    function getOrigin() {
        return $this->origin;
    }
    
    function setOrigin($origin) {
        $this->origin = $origin;
    }
    
    function getTaste() {
        return $this->taste;
    }
    
    function setTaste($taste) {
        $this->taste = $taste;
    }
    
    public function validate() {
        echo "I am from fruit class";
        //WaterMelon::validate();
    }
}

Class Country {
    public $name;
    public $code;
    public function __construct($name, $code) {
        $this->name = $name;
        $this->code = $code;
    }
}

$bd = new Country('Bangladesh', 'BD');
$au = new Country('Australia', 'AU');

$mango = new Fruit('Mango',$bd,'Very good');
$apple = new Fruit('Apple',$au,'Ok');


$test = clone $apple;


print_r($test);
print_r($apple);

$test->setName("Plastic apple");

$test->origin->name = "China";

var_dump($test);
print_r($apple);

$str = serialize($test);

$mizan = unserialize($str);

var_dump($mizan);
