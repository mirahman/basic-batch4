<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$chars = [0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F'];
$indexes = [0 => 0,1 => 1,2 => 2,3 => 3,4 => 4,5=>5,6=>6,7=>7,8 => 8,9 => 9,'A' => 10,'B' => 11,'C' => 12,'D' => 13,'E' => 14,'F' => 15];


$currentNumber = "ABCDF21";
$currentBase = 16;
$targetBase = 2;

function convertToDecimal($currentNumber, $currentBase)
{
    global $chars,$indexes;
    $str = strrev($currentNumber);
    $newNumber = "";
    for($i = 0;$i<strlen($str);$i++)
    {
        $char = substr($str, $i, 1);
        $val =$indexes[$char];              
        $newNumber += $val * $currentBase**$i;
    }            
    return $newNumber;
}


function decimalToAnyBase($decimal, $base)
{
    global $indexes;
    $newNumber = "";
    
    do {
        $reminder = $indexes[$decimal%$base];
        $newNumber = $reminder . "".$newNumber;
        $decimal = (int) ($decimal/$base);
        
    } while($decimal);
    
    return $newNumber;
}

$decimal =  convertToDecimal($currentNumber, $currentBase);

$targetNumber = decimalToAnyBase($decimal, $targetBase);

echo "Target Number is ".$targetNumber;