<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

final Class Fruit {
    
    public $name = "";
    private $origin = "";
    protected $taste = 0;
    
    public $productionYear = "";
    
    
    function __construct($name = "1", $origin ="2", $taste ="3") {
        //echo "I am being called with $name, $origin, $taste <br />";
        $this->name = $name;
        $this->origin = $origin;
        $this->taste = $taste;
    }
    
    function getName() {
        //$this->validate();
        
        return $this->name;
        
    }
    
    function setName($name) {
        $this->name = $name;
    }

    function getOrigin() {
        return $this->origin;
    }
    
    function setOrigin($origin) {
        $this->origin = $origin;
    }
    
    function getTaste() {
        return $this->taste;
    }
    
    function setTaste($taste) {
        $this->taste = $taste;
    }
    
    public final function validate() {
        echo "I am from fruit class";
        //WaterMelon::validate();
    }
}

Class WaterMelon extends Fruit{

    function showParentMethod()
    {
        $this->validate();
    }
    
    public function validate() {
        echo "i am from watermelon";
        //parent::validate();
    }
    
   
}

Class ChtWaterMelon extends WaterMelon {

    
    function validate() {
        echo "i am from chittagong";
        parent::validate();
        Fruit::validate();
    }
}

$f1 = new ChtWaterMelon('Chittagong watermelon','Raujan','Very tasty');
echo $f1->getName();