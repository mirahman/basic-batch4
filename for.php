<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// step 1: i = 1
// step 2: checks if i < 10 if true move to step 3 , else out of loop
// step 3: statement (echo in the example below)
// step 4: i++
// step 5: go to step 2

for($i=1;$i<10;$i++)  {
    echo $i."<br />";
}

for($i = 5; $i <=100;$i = $i + 5):
    echo $i."<br />";
endfor;

/*
for(;;) {
    echo $i;
}
 * 
 */
// a ^ 2 + b ^ 2 = c ^ 2
for($a = 1; $a<50;$a++) {
    for($b = 1; $b<50;$b++) {      
        for($c = 1; $c<50;$c++) {
            if($a * $a + $b * $b == $c * $c) {
                echo $a." - ".$b." - ".$c."<br />";
            }
        }
    } 
}

for($a = 1; $a<=10;$a++) {
    echo "A: ".$a."<br />";
    for($b = 1; $b<=10;$b++) {    
        if($a == $b) continue;
        echo "B: ".$b."<br />";
        for($c = 1; $c<=10;$c++) {
            echo "C: ".$c."<br />";
            if($a * $a + $b * $b == $c * $c) {
                echo $a." - ".$b." - ".$c."<br />";
                break 3;
            }
        }
    } 
}