<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//Public
//Private
//Protected

Class Fruit {
    
    public $name = "";
    private $origin = "";
    protected $taste = 0;
    
    public $productionYear = "";
    
    
    function __construct($name = "", $origin ="", $taste ="") {
        //echo "I am being called with $name, $origin, $taste <br />";
        $this->name = $name;
        $this->origin = $origin;
        $this->taste = $taste;
    }
    
    function getName() {
        return $this->name;
    }
    
    function setName($name) {
        $this->name = $name;
    }

    function getOrigin() {
        return $this->origin;
    }
    
    function setOrigin($origin) {
        $this->origin = $origin;
    }
    
    function getTaste() {
        return $this->taste;
    }
    
    function setTaste($taste) {
        $this->taste = $taste;
    }
}

$mango = new Fruit("Mango","Chapai", "Sweet");

//$mango->name = "My Mango";
$mango->setName("My Mango");

print_r($mango);

echo $mango->getOrigin()."<br />";
echo $mango->getTaste();