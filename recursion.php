<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
n! = n X (n-1)!
5! = 5 X 4!
4! = 4 X 3!
3! = 3 X 2!
2! = 2 X 1!
1! = 1
*/

function factorial($n)
{
    echo "N = ".$n."<br />";
    if($n == 1) return 1;
    
    return $n * factorial($n-1);
}

echo factorial(5);