<?php

namespace Mizan;

Class Banana {
    
    public static function showName() {
        echo "I am Banana from Mizan";
    }
}

Class Mango {
    
    public static function showName() {
        echo "I am Mango from Mizan";
    }
}



namespace TechMasters;

Class Banana {
    
    public static function showName() {
        echo "I am Banana from TechMasters";
    }
}




