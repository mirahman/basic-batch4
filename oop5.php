<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// parent  -> child
// public  -> public
// protected -> protected / public
// private -> private / protected / public

Class Fruit {
    
    public $name = "";
    private $origin = "";
    protected $taste = 0;
    
    public $productionYear = "";
    
    
    function __construct($name = "1", $origin ="2", $taste ="3") {
        //echo "I am being called with $name, $origin, $taste <br />";
        $this->name = $name;
        $this->origin = $origin;
        $this->taste = $taste;
    }
    
    function getName() {
        $this->validate();
        
        return $this->name;
        
    }
    
    function setName($name) {
        $this->name = $name;
    }

    function getOrigin() {
        return $this->origin;
    }
    
    function setOrigin($origin) {
        $this->origin = $origin;
    }
    
    function getTaste() {
        return $this->taste;
    }
    
    function setTaste($taste) {
        $this->taste = $taste;
    }
    
    private function validate() {
        
    }
}


Class Mango extends Fruit{
   public $harvestTime = "";
   
   protected function showMe()
   {
       echo $this->origin;
   }
   
   public function showMePublic() {
       return $this->showMe();
   }
}

Class Banana extends Fruit {
    public $length = "";
}


$lengra = new Mango();

$mango->name = "Jack fruit";

print_r($lengra);

echo $lengra->showMePublic()."<br />";


