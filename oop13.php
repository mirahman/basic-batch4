<?php

// please override all methods in child class

interface Fruit  {
        
    public function setName();
    
    public function showName();
    
}

interface Mango  {
        
    public function setTaste();
    
    public function setOrigin();
    
}


class Lengra implements Fruit, Mango {
    
    public function setName() {
        
    }
    
    public function showName() {
        
    }
    
    public function anotherOne() {
        
    }
    
    public function setTaste() {
        
    }
    
    public function setOrigin() {
        
    }
    
}

//$mango = new Mango();