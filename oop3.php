<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
echo "<pre>";

$name = "outside class";



Class Fruit {
    
    var $name = "";
    public static $type = "Fruit";
    
    const PI = 3.1416;
    
    function showName()
    {
        echo "I am ".$this->getName();
    }
    
    function getName()
    {
        return $this->name;
    }
    
    function setName($name)
    {
        return $this->name = $name;
    }
    
    
    static function showStaticName()
    {
        echo "I have been called without object";
    }
    
    
}

Class Flower {
    
    var $name = "";
    var $type = "Flower";
    
    
    function showName()
    {
        echo "I am ".$this->getName();
    }
    
    function getName()
    {
        return $this->name;
    }
    
    function setName($name)
    {
        return $this->name = $name;
    }
    
}

$mango = new Fruit;
$mango1  = new Fruit();
$mango2 = new Fruit();

$mango->setName("Lengra");
echo $mango->showName();

$mango1->setName("Fazli");
echo $mango1->showName();

$mango2->setName("Him Shagor");
echo $mango2->showName();

print_r($mango);
print_r($mango1);
print_r($mango2);


echo Fruit::$type;
Fruit::showStaticName();

echo Fruit::PI;

$mango->showStaticName();