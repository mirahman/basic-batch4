<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Fruit {
    
    var $name = "fruit";
    
    
    function showName()
    {
        echo "I am fruit";
    }
    
}

Class Flower {
    
    var $name = "flower";
    
    
    function showName()
    {
        return "I am flower";
    }
    
}

$mango = new Fruit();
$rose  = new Flower();

echo $mango->name."<br />";
$mango->showName()."<br />";

echo $rose->name."<br />";
echo $rose->showName()."<br />";

var_dump($mango);
var_dump($rose);