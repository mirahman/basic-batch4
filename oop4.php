<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// constructor


echo "<pre>";

$name = "outside class";



Class Fruit {
    
    var $name = "";
    var $origin = "";
    var $taste = 0;
    
    
    function __construct($name = "", $origin ="", $taste ="") {
        //echo "I am being called with $name, $origin, $taste <br />";
        $this->name = $name;
        $this->origin = $origin;
        $this->taste = $taste;
    }
    
    function __destruct() {
        echo "I am being destroyed <Br />";
    }
    
    function showName()
    {
        echo "I am ".$this->getName()."<br />";
    }
    
    function getName()
    {
        return $this->name;
    }
    
    function setName($name)
    {
        return $this->name = $name;
    }
    
    function getOrigin()
    {
        return $this->origin;
    }
    
    function setOrigin($origin)
    {
        return $this->origin = $origin;
    }
    
    function getTaste()
    {
        return $this->taste;
    }
    
    function setTaste($taste)
    {
        return $this->taste = $taste;
    }
       
    
    function __call($name, $param)
    {
        echo "You are looking for $name, but i do not have it";
    }
    
    
    function __get($name) {
        echo "I have no property named $name";
    }
    
    function __set($name, $value) {
        echo "I cannot set property named $name";
    }
    
    function __isset($name) {
        
    }
    
    
    function __sleep() {
        echo "I am going to serialize the object";
        
        return ['name'];
    }
    
    function __clone() {
        
    }
    
}


$mango = new Fruit('Lengra','Chapai',10);
$mango1  = new Fruit('Fazli',"Rajshahi",7);
$mango2 = new Fruit("Him Shagor","Rajshahi",9);

echo $mango->myFavorite;

$mango->weight = 100;

$mango->showTheName();

print_r($mango);
print_r($mango1);
print_r($mango2);

$newMango = serialize($mango);

$testMango = unserialize($newMango);
print_r($testMango);

$mango = null;
unset($mango1);

echo "Script ends<br />";
/*
$mango->setName("Lengra 101");
echo $mango->showName();
$mango->setOrigin("Chapai");
$mango->setTaste(100);

$mango1->setName("Fazli");
echo $mango1->showName();
$mango1->setOrigin("Rajshahi");
$mango1->setTaste(7);

$mango2->setName("Him Shagor");
echo $mango2->showName();
$mango2->setOrigin("Rajshahi");
$mango2->setTaste(9);

print_r($mango);
print_r($mango1);
print_r($mango2);

*/



if(isset($mango->isFromRajshahi)) {
    
}