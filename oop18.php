<?php


Class Fruit {
    
    public $name = "";
    private $origin = "";
    protected $taste = 0;
    
    public $productionYear = "";
    
    
    function __construct($name = "1", $origin ="2", $taste ="3") {
        //echo "I am being called with $name, $origin, $taste <br />";
        $this->name = $name;
        $this->origin = $origin;
        $this->taste = $taste;
    }
    
    function getName() {
        $this->validate();
        
        return $this->name;
        
    }
    
    function setName($name) {
        $this->name = $name;
        
        return $this;
    }

    function getOrigin() {
        return $this->origin;
    }
    
    function setOrigin($origin) {
        $this->origin = $origin;
        
        return $this;
    }
    
    function getTaste() {
        return $this->taste;
    }
    
    function setTaste($taste) {
        $this->taste = $taste;
    }
    
    private function validate() {
        
    }
}



$mango = new Fruit();

$mango->setName("Fazli");
$mango->setOrigin("Rajshahi");
$mango->setTaste("Not so sweet");


$mango->setName("Fazli")->setOrigin("Rajshahi")->setTaste("Not so sweet");


