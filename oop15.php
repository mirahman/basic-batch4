<?php
interface DeshiFruites{}

interface aussieFruits extends DeshiFruites{}

Class Fruit implements DeshiFruites{
    
    public $name = "";
    private $origin = "";
    protected $taste = 0;
    
    public $productionYear = "";
    
    
    function __construct($name = "1", $origin ="2", $taste ="3") {
        //echo "I am being called with $name, $origin, $taste <br />";
        $this->name = $name;
        $this->origin = $origin;
        $this->taste = $taste;
    }
    
    function getName() {
        $this->validate();
        
        return $this->name;
        
    }
    
    function setName($name) {
        $this->name = $name;
    }

    function getOrigin() {
        return $this->origin;
    }
    
    function setOrigin($origin) {
        $this->origin = $origin;
    }
    
    function getTaste() {
        return $this->taste;
    }
    
    function setTaste($taste) {
        $this->taste = $taste;
    }
    
    private function validate() {
        
    }
}


Class MyFruits {
    public $name;
    public function __construct() {
       //$this->name[] = $name;
    }
    
    public function addFavFruit(DeshiFruites $name)
    {
        $this->name[] = $name;
    }
}

Class Mango implements DeshiFruites{
    
}


$lengra = new Mango;

$mango = new Fruit();

$mango->name = "Mango";

$mizanFav = new MyFruits;
$mizanFav->addFavFruit($mango);
$mizanFav->addFavFruit($lengra);
